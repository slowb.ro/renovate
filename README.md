## Renovate for Slowbro

### How to use

- Add our bot: "slowpoke the bot" to your repository with write access
- Every 15 minutes the bot will check if it needs to do anything
    - Eventually. Once https://github.com/go-gitea/gitea/pull/22751 is merged.
- modify renovate.json in your repo if needed
